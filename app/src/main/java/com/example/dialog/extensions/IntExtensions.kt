package com.example.dialog.extensions

import android.content.res.Resources


val Int.pxToDp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
