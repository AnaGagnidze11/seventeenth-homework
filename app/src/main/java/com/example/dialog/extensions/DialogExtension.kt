package com.example.dialog.extensions

import android.app.Dialog
import android.util.DisplayMetrics
import android.view.Window
import android.view.WindowManager

fun Dialog.setUp(background: Int){
    window!!.setBackgroundDrawableResource(background)
    window!!.requestFeature(Window.FEATURE_NO_TITLE)
    val displayMetrics = DisplayMetrics()
    context.display?.getRealMetrics(displayMetrics)
    window!!.attributes.width = displayMetrics.widthPixels.pxToDp/3
    window!!.attributes.height = displayMetrics.heightPixels.pxToDp/2
}