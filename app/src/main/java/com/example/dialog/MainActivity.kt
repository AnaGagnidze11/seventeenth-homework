package com.example.dialog

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dialog.databinding.ActivityMainBinding
import com.example.dialog.databinding.DialogLayoutBinding
import com.example.dialog.extensions.setUp

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        openDialog()
        setContentView(binding.root)
    }

    private fun openDialog(){
        binding.openDialogButton.setOnClickListener {
            setUpDialog()
        }
    }

    private fun setUpDialog(){
        val dialog = Dialog(this)
        val dialogBinding = DialogLayoutBinding.inflate(layoutInflater)
        dialog.setUp(android.R.color.transparent)
        dialog.setContentView(dialogBinding.root)

        dialogBinding.closingButton.setOnClickListener {
            dialog.cancel()
        }

        dialog.show()
    }
}